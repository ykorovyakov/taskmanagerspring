package ru.volnenko.se.listner;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.volnenko.se.command.event.RawCommandEvent;

@EnableAsync
@Component
public class CommandEventListner {
  @Async
  @EventListener(condition = "#event.getCommandText().equals('help')")
  public void handleHelpCommandCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  @Async
  @EventListener(condition = "#event.getCommandText().contains('task')")
  public void handleTaskCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  @Async
  @EventListener(condition = "#event.getCommandText().contains('project')")
  public void handleProjectCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  @Async
  @EventListener(condition = "#event.getCommandText().contains('bin')")
  public void handleBinDataCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  @Async
  @EventListener(condition = "#event.getCommandText().contains('json')")
  public void handleJsonDataCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  @Async
  @EventListener(condition = "#event.getCommandText().contains('xml')")
  public void handleXmlDataCommand(final RawCommandEvent event) throws Exception {
    handle(event);
  }

  private void handle(final RawCommandEvent event) throws Exception {
    Assert.notNull(event.getCommand(), "The command must not be null");

    event.getCommand().execute();

//    System.out.println(Thread.currentThread().getId());
//    Thread.sleep(10000);
  }
}
