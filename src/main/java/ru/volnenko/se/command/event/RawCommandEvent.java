package ru.volnenko.se.command.event;

import org.springframework.context.ApplicationEvent;
import ru.volnenko.se.command.AbstractCommand;

public class RawCommandEvent extends ApplicationEvent {
  private final String commandText;
  private final AbstractCommand command;

  public RawCommandEvent(final Object source, final AbstractCommand command) {
    super(source);
    this.command = command;
    this.commandText = command.command();
  }

  public String getCommandText() {
    return commandText;
  }

  public AbstractCommand getCommand() {
    return command;
  }
}
