package ru.volnenko.se;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.volnenko.se.controller.Bootstrap;

public class App {
    private static final String APP_CONTEXT_FILENAME = "AppContext.xml";

    public static void main(String[] args) throws Exception {
        final ApplicationContext ctx = new ClassPathXmlApplicationContext(APP_CONTEXT_FILENAME);
        final Bootstrap bootstrap = ctx.getBean("bootstrap", Bootstrap.class);
        bootstrap.start();
    }
}
